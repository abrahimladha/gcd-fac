import sys
import math

p = []
q = []
r = []
f1 = open("primes2.txt",'r')
lines = f1.readlines()
for line in lines:
    p.append(int(line))

prod = 1
for i in range(len(p)):
    prod = p[i]*prod
    q.append(prod)

y = int(sys.argv[1])


#print(p[:20])
#print(q[:20])

if math.gcd(y,2) == 2:
    print("2")
    sys.exit()

if y in p:
    print("prime!")
    sys.exit()


def gcdfac(x):
    L = 0
    R = math.ceil(math.sqrt(x))
    while L <= R:
        m = math.floor((L+R)/2)
        if math.gcd(q[m],x) > 1:
            if math.gcd(q[m-1],x) > 1:
                R = m - 1
            else:
                return p[m]
            break
        elif math.gcd(q[m],x) == 1:
            if math.gcd(q[m+1],x) > 1:
                return p[m+1]
                break
        else:
            L = m+1


print(gcdfac(y))
